package Dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.Employee;

public class EmployeeDao {
	 public List<Employee> findAll() {
	        Connection conn = null;
	        List<Employee> empList = new ArrayList<Employee>();

	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * from user WHERE NOT login_id = 'admin'";

	             // SELECTを実行し、結果表を取得
	            Statement stmt = conn.createStatement();
	            ResultSet rs = stmt.executeQuery(sql);

	            // 結果表に格納されたレコードの内容を
	            // Employeeインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {

	            	 int id = rs.getInt("id");
	            	 String login_id= rs.getString("login_id");
	            	 String name= rs.getString("name");
	            	 String birth_date= rs.getString("birth_date");
	            	 String password= rs.getString("password");
	              	 String create_date= rs.getString("create_date");
	            	 String update_date= rs.getString("update_date");
	            	 Employee employee = new Employee(id, login_id, name, birth_date,password, create_date,update_date);
	            	 empList.add(employee);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
	        return empList;
	    }
	    public Employee findById(String login_id , String password) {
	        Connection conn = null;
	        String result=SecretById(password);
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();
	            // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE login_id = ? and password=? ";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = (PreparedStatement) conn.prepareStatement(sql);
				pStmt.setString(1,  login_id);
				pStmt.setString(2,  password);
	            ResultSet rs = pStmt.executeQuery();
	            if (!rs.next()) {
	                return null;
	            }
	            int id = rs.getInt("id");
	            String login_id2= rs.getString("login_id");
	            String name= rs.getString("name");
	            String birth_date= rs.getString("birth_date");
	            String password2= rs.getString("password");
	            String create_date= rs.getString("create_date");
	            String update_date= rs.getString("update_date");
	            return new Employee(id, login_id2, name, birth_date,password2, create_date,update_date);
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }

	    }
	    public  List<Employee> searchById(String login_id,String name,String startDate,String endDate ) {
	        Connection conn = null;
	        List<Employee> empList = new ArrayList<Employee>();
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();


	            // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE NOT login_id = 'admin'";

	            if(!login_id.equals("")) {
	            	sql += " AND login_id = '" + login_id + "'";
	            }
	            if(!name.equals("")) {
	            	sql += " AND name LIKE '%" + name + "%'";
	            }
	            if((!startDate.equals(""))) {
	            	sql += " AND birth_date >='" + startDate + "'";
	            }

	            if((!endDate.equals(""))){
	            	sql += " AND birth_date <='" + endDate + "'";
	            }
	            // SELECTを実行し、結果表を取得

	            PreparedStatement pStmt = (PreparedStatement) conn.prepareStatement(sql);

	            ResultSet rs = pStmt.executeQuery();
	            // 結果表に格納されたレコードの内容を
	            // Employeeインスタンスに設定し、ArrayListインスタンスに追加
	            while (rs.next()) {
	            	 int id = rs.getInt("id");
	            	 String login_id2= rs.getString("login_id");
	            	 String name2= rs.getString("name");
	            	 String birth_date= rs.getString("birth_date");
	            	 String password= rs.getString("password");
	              	 String create_date= rs.getString("create_date");
	            	 String update_date= rs.getString("update_date");
	            	 Employee employee = new Employee(id, login_id2, name2, birth_date,password, create_date,update_date);
	            	 empList.add(employee);
	            }
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        } finally {
	            if (conn != null) {
	                try {
	                    conn.close();
	                } catch (SQLException e) {
	                    e.printStackTrace();
	                    return null;
	                }
	            }
	        }
			return  empList;
	    }


	    public  void insertById(String login_id , String name , String password  ,  String birth_date ) {
	        Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "INSERT INTO  user(login_id,password,name,birth_date,create_date,update_date)VALUES(?,?,?,?,now(),now())";
	            String result=SecretById(password);

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1,  login_id);
				pStmt.setString(2,  result);
				pStmt.setString(3,  name);
				pStmt.setString(4,  birth_date);
	            pStmt.executeUpdate();

	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {

	        }

	    }
	    public Employee infoById(String id) {
	        Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE id = ?";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = (PreparedStatement) conn.prepareStatement(sql);
				pStmt.setString(1,  id);
	            ResultSet rs = pStmt.executeQuery();
	            if (!rs.next()) {
	                return null;
	            }
	            int id2 = rs.getInt("id");
	            String login_id2= rs.getString("login_id");
	            String name= rs.getString("name");
	            String birth_date= rs.getString("birth_date");
	            String password2= rs.getString("password");
	            String create_date= rs.getString("create_date");
	            String update_date= rs.getString("update_date");
	            return new Employee(id2, login_id2, name, birth_date,password2, create_date,update_date);
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }

	    }
	    public Employee alreadyById(String login_id) {
	        Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "SELECT * FROM user WHERE login_id = ? ";

	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = (PreparedStatement) conn.prepareStatement(sql);
				pStmt.setString(1,  login_id);
	            ResultSet rs = pStmt.executeQuery();
	            if (!rs.next()) {
	                return null;
	            }
	            int id = rs.getInt("id");
	            String login_id2= rs.getString("login_id");
	            String name= rs.getString("name");
	            String birth_date= rs.getString("birth_date");
	            String password2= rs.getString("password");
	            String create_date= rs.getString("create_date");
	            String update_date= rs.getString("update_date");
	            return new Employee(id, login_id2, name, birth_date,password2, create_date,update_date);
	        } catch (SQLException e) {
	            e.printStackTrace();
	            return null;
	        }

	    }


	    public void updateById(String login_id , String name , String password) {
	        Connection conn = null;
	        try {
	        	if(password.equals("")) {
	        		// データベースへ接続
		            conn = DBManager.getConnection();

		            // SELECT文を準備
		            String sql = "UPDATE user SET name= ?,update_date=now() WHERE login_id = ?";
		             // SELECTを実行し、結果表を取得
		            PreparedStatement pStmt = conn.prepareStatement(sql);
					pStmt.setString(1,  name);
					pStmt.setString(2,  login_id);
					pStmt.executeUpdate();
					return;
		        }else{
		            // データベースへ接続
		            conn = DBManager.getConnection();
		            String result=SecretById(password);

		            // SELECT文を準備
		            String sql = "UPDATE user SET password = ?,name= ?  ,update_date=now() WHERE login_id = ?";
		             // SELECTを実行し、結果表を取得
		            PreparedStatement pStmt = conn.prepareStatement(sql);
		            pStmt.setString(1,  result);
					pStmt.setString(2,  name);
					pStmt.setString(3,  login_id);
					pStmt.executeUpdate();
					return;
		        }

	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {

	        }
	    }

		public void deleteById(String login_id) {
			Connection conn = null;
	        try {
	            // データベースへ接続
	            conn = DBManager.getConnection();

	            // SELECT文を準備
	            String sql = "DELETE FROM user WHERE login_id  = ?";
	             // SELECTを実行し、結果表を取得
	            PreparedStatement pStmt = conn.prepareStatement(sql);
				pStmt.setString(1,  login_id);
				 pStmt.executeUpdate();
	        } catch (SQLException e) {
	            e.printStackTrace();
	        } finally {

	        }

		}
		public String SecretById(String password) {
			String source = password;
			//ハッシュ生成前にバイト配列に置き換える際のCharset
			Charset charset = StandardCharsets.UTF_8;
			//ハッシュアルゴリズム
			String algorithm = "MD5";

			//ハッシュ生成処理
			byte[] bytes;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
				String result = DatatypeConverter.printHexBinary(bytes);
				return result;
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
			return algorithm;

		}


}
