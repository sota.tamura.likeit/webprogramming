<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="model.Employee"%>
<%@page import="Dao.EmployeeDao"%>
<!DOCTYPE html>
<html>
<head>
<link rel="UserStyle" href="UserStyle.css">
<title>ユーザー一覧</title>
</head>
<body>
	<%
		Employee employee = (Employee) session.getAttribute("employeeInfo");
	%>
	<header>
		<div class="top">

			<p>
				<%=employee.getName()%>さん
			</p>

			<a href="LogoutServlet">ログアウト</a>
		</div>
	</header>


	<h1 style="text-align: center">ユーザー一覧</h1>
	<a href="NewUserServlet">新規登録</a>
	<div class="body-main" style="text-align: center">
		<form action="UserServlet" method="post">
			<p>
				ログインID<input type="text" name="LoginId">
			</p>
			<p>
				ユーザー名<input type="text" name="name">
			</p>
			<p>
				生年月日<input type="date" name=startDate value="　年/　月/　日">～
			<p>
				生年月日<input type="date" name=endDate value="　年/　月/　日">
			</p>
			<p>
				<input type="submit" value="検索">
			</p>
		</form>
		<div class="table-class">
			<table style="text-align: center">
				<tr>
					<th>ログインID</th>
					<th>ユーザ名</th>
					<th>生年月日</th>
					<th>&emsp;</th>
				</tr>
				<c:forEach var="rs" items="${rs}">
					<tr>
						<th>${rs.login_id}</th>
						<td>${rs.name}</td>
						<td>${rs.birth_date}</td>

						<td><c:if test="${employeeInfo.id==1}">
								<button type="button" name="radio"
									onclick="location.href='InfoServlet?id=${rs.id}'" value="chack">詳細</button>
								<button type="button" name="radio"
									onclick="location.href='UpdateServlet?id=${rs.id}'"
									value="update">更新</button>
								<button type="button" name="radio"
									onclick="location.href='DeleteServlet?id=${rs.id}'"
									value="delete">削除</button>
							</c:if> <c:if test="${employeeInfo.id!=1}">
								<button type="button" name="radio"
									onclick="location.href='InfoServlet?id=${rs.id}'" value="chack">詳細</button>
								<c:if test="${employeeInfo.login_id==rs.login_id}">
									<button type="button" name="radio"
										onclick="location.href='UpdateServlet?id=${rs.id}'"
										value="update">更新</button>
								</c:if>
							</c:if></td>

					</tr>
				</c:forEach>
			</table>
		</div>
	</div>
</head>
</html>