<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="model.Employee"%>
<%@page import="Dao.EmployeeDao"%>
<%Employee employee = (Employee) session.getAttribute("employeeInfo"); %>
<!DOCTYPE>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="UpdateStyle" href="UpdateStyle.css">
        <title>更新</title>
    </head>
     <body>
         <header>

              <div class="top">

    		 <p> <%= employee.getName() %>さん</p>

             <a href="LogoutServlet">ログアウト</a>
             </div>
         </header>
         <div class="body-main" style="text-align:center">
         <h1 class="body-logo">ユーザ情報更新</h1>
                  <c:if test="${errMsg != null}" >
   		         <div class="error">${errMsg}</div>
                </c:if>
         <form action="UpdateServlet" method="post">
         <div class="body-list">
           　<div class="login-id">ログインID<p class="id">${update.login_id}</p></div>
             <input type="hidden"  name="loginid" value="${update.login_id}">
          	 <div class="password">パスワード<input type="password" name="pass"placeholder=""></div>
             <div class="again-password">パスワード(確認）<input type="password" name="repass"></div>
          	 <div class="username">ユーザ名 <input type="text" name="name" placeholder="${update.name}"></div>
        </div>
          <p><input type="submit" value="更新"></p>
          </form>

          </div>
     </body>

     <footer>
          <a href="UserServlet">戻る</a>
     </footer>
</html>