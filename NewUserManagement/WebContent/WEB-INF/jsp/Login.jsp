<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
        <link rel="Loginstyle" href="LoginStyle.css">
        <title>	ログイン</title>
    </head>

    <body>
    <form action="LoginServlet" method="post"style="text-align:center">
    <div class="body-wrapper">
    	<c:if test="${errMsg != null}" >
       <div class="error">${errMsg}</div>
  	    </c:if>
        <h1>ログイン画面</h1>
        <p>ログインID：<input type="text" name="name"></p>
        <p>パスワード：<input type="password" name="pass"></p>
        <p><input type="submit" value="ログイン"></p>
    </div>
    </form>


    </body>
</html>