<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="model.Employee"%>
<%@page import="Dao.EmployeeDao"%>
<%Employee employee = (Employee) session.getAttribute("employeeInfo"); %>
<!DOCTYPE>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="DeleteStyle" href="DeleteStyle.css">
        <title>削除</title>
    </head>
     <body>
         <header>
             <div class="top">
    		 <p> <%= employee.getName() %>さん</p>
             <a href="LogoutServlet">ログアウト</a>
             </div>
        </header>
        <div class="body-main" style="text-align:center">
   	          <h1 class="body-logo">ユーザ削除確認</h1>
  			 <div class="login-id">ログインID<p class="id">${delete.login_id}</p>
		　   <p>を本当に削除してもよろしいでしょうか</p></div>
   			 <button type="button"class="cancell" name="cancell" onclick="location.href='UserServlet'" value="chack">キャンセル</button>
   			<form action="DeleteServlet" method="post">
   			<input type="hidden"  name="loginid" value="${delete.login_id}">
   　　　　　<button type="submit" class="ok" name="ok"  value="OK">OK</button>
   			</form>
   		</div>

    </body>
