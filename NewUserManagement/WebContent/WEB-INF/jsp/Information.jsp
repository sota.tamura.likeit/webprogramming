<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@page import="Dao.EmployeeDao"%>
<!DOCTYPE>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="InformationStyle" >
        <title>詳細</title>
    </head>
    <body style="text-align:center">
         <header>

              <div class="top">

    		 <p> ${info.name}さん</p>

             <a href="LogoutServlet">ログアウト</a>
             </div>
         </header>
         <div class="body-main" style="text-align:center">

        <h1>ユーザ情報詳細参照</h1>
        <div class="main-list">

            <div class="login-id">ログインID<p class="id">${info.login_id}</p></div>
            <div class="body-name">ユーザ名  <p class="name">${info.name}</p></div>
            <div class="birthday">生年月日 <p class="birthday">${info.birth_date}</p></div>
            <div class="create_date">登録日時<p class="start"></p>${info.create_date}</div>
            <div class="update_date">更新日時<p class="update"></p>${info.update_date}</div>
        </div>
         </div>
        <footer>
         <a href="UserServlet">戻る</a>
     </footer>

    </body>
</html>