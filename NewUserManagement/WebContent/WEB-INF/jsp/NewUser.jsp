<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@page import="model.Employee"%>
<%@page import="Dao.EmployeeDao"%>
<%Employee employee = (Employee) session.getAttribute("employeeInfo"); %>
<!DOCTYPE>
<html>
    <head>
        <meta charset="utf-8">
        <link rel="NewUserStyle" href="NewUserStyle.css">
        <title>	新規登録</title>
    </head>
    <body >
        <header>
    		 <div class="top">

    		 <p> <%= employee.getName() %>さん</p>

             <a href="LogoutServlet">ログアウト</a>
             </div>
         </header>
         <div class="body-main" style="text-align:center">
         <h1 class="body-logo">ユーザ新規登録</h1>
          <c:if test="${errMsg != null}" >
         <div class="error">${errMsg}</div>
       </c:if>
         <div class="body-list">
             <div class="body-left">
             <form action="NewUserServlet" method="post">
                <div class="login-id">ログインID<p><input type="text" name="id"></p></div>
            	<div class="password">パスワード            <p><input type="password" name="pass"></p></div>
            	<div class="again-password">パスワード(確認)<p><input type="password" name="repass"></p></div>
                <div class="body-name">ユーザ名 <p><input type="text" name="name"></p></div>
                <div class="birthday">生年月日 <p><input type="date" name="birthday"></p></div>
                <p><input type="submit" value="登録"></p>
                </form>
             </div>
         </div>

       </div>
     </body>
     <footer>
         <a href="UserServlet">戻る</a>
     </footer>
</html>